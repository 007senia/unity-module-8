using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Task1Script : MonoBehaviour
{
    public int N;
    public float Speed;
    public bool Go;

    private Vector3[] positionArray;

    private Vector3 target;
    private bool forward = true;
    private int forwardCounter;
    private int reverseCounter;
    // Start is called before the first frame update
    void Start()
    {
        forwardCounter = 0;
        reverseCounter = N - 1;

        positionArray = new Vector3[N];

        for (int i = 0; i < N; i++)
        {
            positionArray[i].Set(Random.Range(-10, 10), 0, Random.Range(-10, 10));
            Debug.Log(positionArray[i]);
        }

        target = positionArray[0];
    }

    // Update is called once per frame
    void Update()
    {
        if (Go)
            transform.position = Vector3.MoveTowards(transform.position, target, Time.deltaTime * Speed);

        if (transform.position == target)
        {
            if (forward)
            {
                if (target == positionArray[N - 1])
                {
                    forward = false;
                    forwardCounter = 0;
                }
                else
                {
                    target = positionArray[forwardCounter + 1];
                    forwardCounter++;

                    transform.LookAt(target);
                }
            }
            else
            {
                if (target == positionArray[0])
                {
                    forward = true;
                    reverseCounter = N - 1;
                }
                else
                {
                    target = positionArray[reverseCounter - 1];
                    reverseCounter--;

                    transform.LookAt(target);
                }
            }
        }
    }
}
