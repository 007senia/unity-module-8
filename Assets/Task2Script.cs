using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Task2Script : MonoBehaviour
{

    public GameObject[] Cubes;

    public float Speed;
    public float PassDistance;

    private Vector3 target;
    private Vector3 runnerPosition;
    private bool runner;
    private int counter;
    // Start is called before the first frame update
    void Start()
    {
        counter = 0;

        target = Cubes[counter + 1].transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        Cubes[counter].transform.position = Vector3.MoveTowards(Cubes[counter].transform.position, target, Time.deltaTime * Speed);
        
        if (Vector3.Distance(target, Cubes[counter].transform.position) <= PassDistance)
        {
            if (target == Cubes[Cubes.Length - 1].transform.position)
            {
                target = Cubes[0].transform.position;

                counter++;

                Cubes[counter].transform.LookAt(target);
            }
            else if (target == Cubes[0].transform.position)
            {
                counter = 0;
                target = Cubes[counter + 1].transform.position;

                Cubes[counter].transform.LookAt(target);
            }
            else
            {
                counter++;
                target = Cubes[counter + 1].transform.position;

                Cubes[counter].transform.LookAt(target);
            }
        }
        
    }
}